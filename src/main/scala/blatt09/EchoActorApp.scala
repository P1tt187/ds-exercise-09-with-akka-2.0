package blatt09

import akka.actor._
import akka.routing.RoundRobinRouter
import akka.util.Duration
import akka.util.duration._
import com.typesafe.config.ConfigFactory

object EchoActorApp extends App {
  //load config for echo actors and register as echo-service
  val interActor = ActorSystem("echoActorSystem", ConfigFactory.load.getConfig("echoActor")).actorOf(Props[EchoActor], "echo-service")

}