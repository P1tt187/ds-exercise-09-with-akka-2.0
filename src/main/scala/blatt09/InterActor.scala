package blatt09

import java.util.concurrent.TimeUnit
import akka.actor.actorRef2Scala
import akka.actor.Actor
import akka.dispatch.Await
import akka.pattern.ask
import akka.util.Timeout
import scala.actors.threadpool.TimeoutException
import akka.pattern.AskTimeoutException

class InterActor extends Actor {
  // call the remote actor
  private val echoActor = context.actorFor("akka://echoActorSystem@localhost:1234/user/echo-service")

  override def preStart = self ! NextStep
  implicit val timeout: Timeout = Timeout(10000, TimeUnit.MILLISECONDS)

  def receive = {
    case NextStep =>

      val line = readLine()
      try {
        println(Await.result(echoActor ? line, timeout.duration))
      } catch {
        case t: AskTimeoutException => println("Timeout")
        case e => e.printStackTrace();
      }
      self ! NextStep
    case _ =>
  }

  private case object NextStep
}