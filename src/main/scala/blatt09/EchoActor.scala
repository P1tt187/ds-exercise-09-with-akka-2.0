package blatt09

import akka.actor.Actor

class EchoActor extends Actor {

  def receive = {
    case s: String =>
      println(s)

      /**uncomment to test timeout*/
      //Thread.sleep(20000)
      context.sender ! s.reverse // <-- self.channel ! s.reverse 
    case _ => sender ! "Please send a String"
  }

}
