package blatt09

import akka.actor._
import com.typesafe.config.ConfigFactory

object InterActorApp extends App {
  // load the interactor config
  val interActor = ActorSystem("InterActor", ConfigFactory.load.getConfig("interActor")).actorOf(Props[InterActor], "interActor-thing")

}